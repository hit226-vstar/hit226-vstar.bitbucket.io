const btn = document.querySelector("button");

//Create object to watch changes to isReveal variables
let revealPassword = {
  isReveal: false,
  revealListener: (state) => {},
  set reveal(state) {
    this.isReveal = state;
    this.revealListener(state);
  },
  get reveal() {
    return this.isReveal;
  },
  registerListener: function (listener) {
    this.revealListener = listener;
  },
};

//Listen to variable changes - trigger everytime isReveal changes.
revealPassword.registerListener((state) => {
  if (state) {
    document.getElementById("password").textContent = localStorage.getItem(
      "password"
    );
    btn.textContent = "Hide Password";
  } else {
    document.getElementById("password").textContent = passwordMasking(
      localStorage.getItem("password")
    );
    btn.textContent = "Reveal Password";
  }
});

//Format phone number
const formatPhone = (num) => {
  num = num.split("");
  return `${num.slice(0, 3).join("")} ${num.slice(3).join("")}`;
};
//Format Birthdate.
const formatDate = (date) => {
  if (!date) {
    return;
  }
  const day = date.substr(-2);
  const month = date.substr(5, 2);
  const year = date.substr(0, 4);
  return `${day}-${month}-${year}`;
};
//Password masking
const passwordMasking = (password) => {
  password = password.split("");
  password.forEach((char, i) => {
    password[i] = "*";
  });
  return password.join("");
};

//Toggle password state.
btn.onclick = () => {
  revealPassword.reveal = !revealPassword.reveal;
};

//Fill submitted data.
document.getElementById("name").textContent = localStorage.getItem("name");
document.getElementById("address").textContent =
  localStorage.getItem("address") || "No address submitted";
document.getElementById("email").textContent =
  localStorage.getItem("email") || "No email submitted";
document.getElementById("password").textContent =
  passwordMasking(localStorage.getItem("password")) || "No password submitted";
document.getElementById("telephoneNum").textContent = `0${localStorage.getItem(
  "countryCode"
)} ${formatPhone(localStorage.getItem("telephoneNum"))}`;
document.getElementById("birthDate").textContent =
  formatDate(localStorage.getItem("birthDate")) || "No birthdate submitted";
