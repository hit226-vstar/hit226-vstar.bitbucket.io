document.addEventListener("DOMContentLoaded", () => {
  const form = document.querySelector("form");
  // Form data
  const name = document.getElementById("name");
  const address = document.getElementById("address");
  const email = document.getElementById("email");
  const password = document.getElementById("password");
  const countryCode = document.getElementById("country-code");
  const telephoneNum = document.getElementById("tel-number");
  const birthDate = document.getElementById("birthdate");
  const errorMsg = document.getElementById("error-message");

  // Calculate the age based on input value
  const calculateAge = (dateValue) => {
    const now = new Date();
    const date = new Date(dateValue);
    //   Difference between present time and input date
    const diff = Math.abs(now - date.getTime());
    //   convert to age (years)
    return Math.floor(diff / (1000 * 60 * 60 * 24 * 365));
  };

  //Return true when pass all validation, false otherwise
  const formValidation = (countryCode, telephoneNum, age) => {
    if (countryCode.value.length < 2) {
      errorMsg.textContent = "Please enter country code";
      return false;
    } else if (telephoneNum.value.length != 7) {
      errorMsg.textContent =
        "Your phone is not a valid phone number, please enter again";
      return false;
    } else if (age <= 13 || age > 50) {
      errorMsg.textContent = "Age must be between 13 and 50";
      return false;
    }
    return true;
  };

  //Run when submit form
  form.onsubmit = (event) => {
    const age = calculateAge(birthDate.value);
    //Prevent form submit on fail validation, show error message
    if (!formValidation(countryCode, telephoneNum, age)) {
      event.preventDefault();
      errorMsg.style.visibility = "visible";
    } else {
      // Saving Data to LocalStorage when form submitted
      localStorage.setItem("name", name.value);
      localStorage.setItem("address", address.value);
      localStorage.setItem("email", email.value);
      localStorage.setItem("password", password.value);
      localStorage.setItem("countryCode", countryCode.value);
      localStorage.setItem("telephoneNum", telephoneNum.value);
      localStorage.setItem("birthDate", birthDate.value);
    }
  };
});
