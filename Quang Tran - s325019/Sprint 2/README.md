# Registration Form

## Overview

Fill in data into the form, hit submit will send data to "read.html" where it will be presented to the users.

### Fields

- Name: required.
- Address: optional.
- Email: optional.
- Password: required.
- Telephone number: required - country code and number separated.
- Birthdate: optional.

### Validation

- Email: should include '@example.com'.
- Telephone Number: should include country code and correct number format.
- Birthdate: user should be between 13 and 50 years old in order to successfully submit data.

### Form views

1. Desktop View

![Desktop](./images/desktop.png)

2. Galaxy S5 View

![GalaxyS5](./images/Galaxy-s5.png)

3. Iphone 6/7/8 Plus View

![Iphone 6/7/8 Plus](./images/IPhone678Plus.png)

4. IPad view

![Ipad](./images/IPad.png)

5. Lighthouse Score

![Lighthouse](./images/Lighthouse.png)

## Notes

1. Required fields marked with asterisks

2. Live server simulation with the site will generate a 405 error on form submitting, recommend to use on client-side only.

3. Form datas will be saved in LocalStorage, sessionStorage can be used here, but will be deleted when users closed the current tab. To clear LocalStorage, users can clear cookies and related browsing datas.

4. The form uses built-in HTML5 validations, some aspects such as "pattern" attribute will not work on Opera Mini, Safari and old browsers versions. Poly-fills are not implemented yet.

5. Lighthouse Report can only generated with a live server simulation. Performance score can vary based on Internet speed.
