
function formValidation()
{

var dob= document.myForm.date;



    if(ValidateDOB(dob)){
        return true;
}

return false;
}



        function ValidateDOB(dob) {
            var r = new Date();
            dobcheck = dob.value.trim();
            y = parseInt(dobcheck.substr(0, 4))
            var dobformat = /^([0-9]{4})\-([0-9]{2})\-([0-9]{2})$/;
            if (dob.value.match(dobformat)) {
                if ((parseInt(y) + 13) >= r.getFullYear()) {
                    alert("Sorry, you are not allowed to register when you are under 13 years old.");
                } else if ((parseInt(y) + 50) <= r.getFullYear()) {
                    alert("Sorry, you are not allowed to register when you are older than 50 years old.");
                } else {
                    return true;
                }
            } else {
                alert("You have entered an invalid DOB, please input DD/MM/YYYY");
                dob.focus();
                return false;
            }
           
        }