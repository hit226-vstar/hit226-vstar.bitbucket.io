function ageValidation(age) {
  if (typeof age !== "number") {
    alert("Please Enter Numeric Values Only");
    return;
  }
  if (age < 13) {
    alert(`Your age is below 13.`);
    return;
  } else if (age > 50) {
    alert(`Your age is above 50.`);
    return;
  }
  return true;
}

function validateForm() {
  var birthdate = document.getElementById("birthdate").value;
  var division = 1000 * 60 * 60 * 24 * 365;
  var input_date = new Date(birthdate);
  var age = Math.floor((new Date() - input_date) / division);
  
  if (!ageValidation(age)) {
    event.preventDefault();
  } else if (ageValidation(age)) {
    alert(`Your age is ${age} and you are eligible to enter.`);
  }
}
