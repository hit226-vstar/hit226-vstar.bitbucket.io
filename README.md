# MN đọc file readme này giúp Quang

1.  Clone repo này về local, mỗi ng đều phải có version riêng của repo này trên máy.
1.  Main codebase cho group assignment sẽ ở **ngoài** folder riêng.
1.  Folder riêng (có tên của mỗi người) chủ yếu để chứa bài tập yêu cầu (coi trên [Learnline](https://online.cdu.edu.au/ultra/courses/_49620_1/outline))
1.  Commit message nên chứa tên của mỗi người + miêu tả ngắn gọn về những thay đổi trong repo.
1.  Tài liệu của team sẽ chủ yếu up lên Space trên confluence, repo này để chứa source code.

---
  

# Please read:

1.  Clone this repo, each member should have their own version on their local computer.
2.  Group assignment main codebase will be put **outside** of the individual folders.
3.  Individual folders (which have their name on it) mainly used for [Learnline](https://online.cdu.edu.au/ultra/courses/_49620_1/outline) exercises.
4.  Commit message nên chứa tên của mỗi người + miêu tả ngắn gọn về những thay đổi trong repo.
5.  Team’s document (charter, bios, etc.) will be posted on Confluence’s Team Space, not this repo.

---

#### For reference

- [How to clone a Bitbucket repo](https://support.atlassian.com/bitbucket-cloud/docs/clone-a-repository/)
- [How to push code changes to Bitbucket repo](https://support.atlassian.com/bitbucket-cloud/docs/push-code-to-bitbucket/)
- [How to update your local repo with remote repo](https://support.atlassian.com/bitbucket-cloud/docs/pull-code-from-bitbucket/)

##### Note:
Use [Git fetch](https://www.atlassian.com/git/tutorials/syncing/git-fetch) instead of Git pull if you want to avoid conflicting between local files and remote files.
